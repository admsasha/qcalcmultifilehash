#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>

#include "version.h"
#include "mainwindow.h"

#include <iostream>

int main(int argc, char *argv[]){
    for (int i=0;i<argc;i++){
        if (!strcmp(argv[i],"--version")){
            std::cout << "version: " << VERSION << std::endl;
            return 0;
        }
    }

    QApplication app(argc, argv);

    QTranslator translator;
    QTranslator qtTranslator;
    QString locale = QLocale::system().name();

    qtTranslator.load("qt_"+locale,QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    if (translator.load(QString("qcalcmultifilehash_") + locale)==false){
        translator.load(QString("/usr/share/qcalcmultifilehash/qcalcmultifilehash_") + locale);
    }
    app.installTranslator(&translator);

    MainWindow form;
    form.show();

    return app.exec();
}
