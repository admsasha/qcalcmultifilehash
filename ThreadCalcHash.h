#ifndef THREADCALCHASH_H
#define THREADCALCHASH_H

#include <QThread>

enum class HASH_ALGORITHM : int {
    Crc8,
    Crc32,
    Md4,
    Md5,
    Sha1,
    Sha224,
    Sha256,
    Sha384,
    Sha512,
};

class ThreadCalcHash : public QThread {
    Q_OBJECT
    public:
        ThreadCalcHash();
        ~ThreadCalcHash();
        void run();

        void setFilename(QString filename);
        void setHashAlgorithm(HASH_ALGORITHM algorithm);
        void setNumber(int number);

    private:
        QString _filename;
        HASH_ALGORITHM _hashAlgorithm;
        int _number;

        std::string getCrc8FromFile(const std::string &filename);
        std::string getCrc32FromFile(const std::string &filename);
        std::string getMD4FromFile(const std::string &filename);
        std::string getMD5FromFile(const std::string &filename);
        std::string getSHA1FromFile(const std::string &filename);
        std::string getSHA224FromFile(const std::string &filename);
        std::string getSHA256FromFile(const std::string &filename);
        std::string getSHA384FromFile(const std::string &filename);
        std::string getSHA512FromFile(const std::string &filename);

    private slots:

    signals:
        void changeValue(int number,int);
        void result(int number,QString);

};

#endif // THREADCALCHASH_H
