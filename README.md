# README #
Calculator hash (SHA1, SHA-224, SHA-256, SHA-384, SHA-512, MD5, CRC32, CRC8)

![QCalcMultiFileHash images](http://dansoft.krasnokamensk.ru/data/1019/qcalcmultifilehash.png)


### How do I get set up? ###
qmake

lupdate ./QCalcMultiFileHash.pro

lrelease ./QCalcMultiFileHash.pro

mkdir /usr/share/qcalcmultifilehash/

cp *.qm /usr/share/qcalcmultifilehash/

make

### Who do I talk to? ###
email: dik@inbox.ru
