#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private:
        Ui::MainWindow *ui;

    private slots:
        void calcHash(int number, QString filename, int algorithm);
        void openfile();
        void exportToFile();

        void calcHashChangeValue(int number, int value);
        void calcHashResult(int number,QString hash);
};

#endif // MAINWINDOW_H
