<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <source>Algorithm:</source>
        <translation>Алгоритм</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="36"/>
        <source>Add file</source>
        <translation>Добавить файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <location filename="mainwindow.cpp" line="89"/>
        <source>Export to file</source>
        <translation>Экпорт в файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>filename</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="75"/>
        <source>Algorithm</source>
        <translation>Алгоритм</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="80"/>
        <source>process</source>
        <translation>Процесс</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="85"/>
        <source>hash</source>
        <translation>Хэшь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="93"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="100"/>
        <source>Copyright DanSoft. All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="54"/>
        <source>Open file</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="54"/>
        <source>All Files (*.*)</source>
        <translation>Все файлы (*.*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="89"/>
        <source>cvs (*.cvs)</source>
        <translation>cvs (*.cvs)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="111"/>
        <source>export to file</source>
        <translation>Экспорт в файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="111"/>
        <source>Export completed successfully</source>
        <translation>Экспорт успешно завершен</translation>
    </message>
</context>
</TS>
