#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QFileDialog>
#include <QDateTime>
#include <QProgressBar>
#include <QMessageBox>
#include <QTextStream>

#include "ThreadCalcHash.h"
#include "version.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("QCalcMultiFileHash "+QString(VERSION)+" ("+QString(DATE_BUILD)+")");
    //this->setFixedSize(this->width(),this->height());

    ui->comboBox->addItem("CRC-8",(int)HASH_ALGORITHM::Crc8);
    ui->comboBox->addItem("CRC-32",(int)HASH_ALGORITHM::Crc32);
    ui->comboBox->addItem("MD4",(int)HASH_ALGORITHM::Md4);
    ui->comboBox->addItem("MD5",(int)HASH_ALGORITHM::Md5);
    ui->comboBox->addItem("SHA-1",(int)HASH_ALGORITHM::Sha1);
    ui->comboBox->addItem("SHA-224",(int)HASH_ALGORITHM::Sha224);
    ui->comboBox->addItem("SHA-256",(int)HASH_ALGORITHM::Sha256);
    ui->comboBox->addItem("SHA-384",(int)HASH_ALGORITHM::Sha384);
    ui->comboBox->addItem("SHA-512",(int)HASH_ALGORITHM::Sha512);
    ui->comboBox->setCurrentIndex(0);


    ui->tableWidget->setColumnWidth(0,200);
    ui->tableWidget->setColumnWidth(2,100);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(3,QHeaderView::Stretch);


    ui->pushButton_3->setEnabled(false);

    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(ui->pushButton_2,SIGNAL(clicked(bool)),this,SLOT(openfile()));
    connect(ui->pushButton_3,SIGNAL(clicked(bool)),this,SLOT(exportToFile()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openfile(){
    QStringList fileNames = QFileDialog::getOpenFileNames(this,tr("Open file"), "", tr("All Files (*.*)"));
    if (fileNames.size() <= 0) return;

    for (QString filename:fileNames){
        int currentCount = ui->tableWidget->rowCount();
        ui->tableWidget->setRowCount(currentCount+1);


        QLabel *label = new QLabel();
        label->setText(filename);
        label->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
        ui->tableWidget->setCellWidget(currentCount,0,label);

        QLabel *labelAlg = new QLabel();
        labelAlg->setText(ui->comboBox->currentText());
        ui->tableWidget->setCellWidget(currentCount,1,labelAlg);

        QProgressBar *p = new QProgressBar();
        p->setValue(24);
        p->setAlignment(Qt::AlignCenter);
        p->setTextDirection(QProgressBar::TopToBottom);
        ui->tableWidget->setCellWidget(currentCount,2,p);

        QLabel *labelHash = new QLabel();
        labelHash->setText("");
        labelHash->setTextInteractionFlags(Qt::LinksAccessibleByMouse|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        ui->tableWidget->setCellWidget(currentCount,3,labelHash);


        ui->pushButton_3->setEnabled(false);
        calcHash(currentCount,filename,ui->comboBox->currentData().toInt());
    }
}

void MainWindow::exportToFile(){
    QString fileName = QFileDialog::getSaveFileName(this,tr("Export to file"), "", tr("cvs (*.cvs)"));
    if (fileName.size() <= 0) return;

    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return;

    QTextStream out(&file);
    for (int i=0;i<ui->tableWidget->rowCount();i++){
        QWidget *wFileName = ui->tableWidget->cellWidget(i,0);
        QLabel *lblFileName = reinterpret_cast<QLabel*>(wFileName);

        QWidget *wAlgHash = ui->tableWidget->cellWidget(i,1);
        QLabel *lblAlgHash = reinterpret_cast<QLabel*>(wAlgHash);

        QWidget *wHash = ui->tableWidget->cellWidget(i,3);
        QLabel *lblHash = reinterpret_cast<QLabel*>(wHash);

        out << lblFileName->text() << ";" << lblAlgHash->text() << ";" << lblHash->text() << "\n";

    }
    file.close();

    QMessageBox::information(this,tr("export to file"),tr("Export completed successfully"));
}

void MainWindow::calcHashChangeValue(int number,int value) {
    QWidget *w = ui->tableWidget->cellWidget(number,2);
    QProgressBar *l = reinterpret_cast<QProgressBar*>(w);
    l->setValue(value);
}

void MainWindow::calcHashResult(int number, QString hash) {
    QWidget *w = ui->tableWidget->cellWidget(number,3);
    QLabel *l = reinterpret_cast<QLabel*>(w);
    l->setText(hash);
    l->setToolTip(hash);

    bool isAllEnded=true;
    for (int i=0;i<ui->tableWidget->rowCount();i++){
        QWidget *wHash = ui->tableWidget->cellWidget(i,3);
        QLabel *lblHash = reinterpret_cast<QLabel*>(wHash);

        if (lblHash->text()=="") isAllEnded=false;
    }

    if (isAllEnded==true){
        ui->pushButton_3->setEnabled(true);
    }
}


void MainWindow::calcHash(int number,QString filename,int algorithm){

    ThreadCalcHash *thCaclHash = new ThreadCalcHash();
    thCaclHash->setFilename(filename);
    thCaclHash->setHashAlgorithm(HASH_ALGORITHM(algorithm));
    thCaclHash->setNumber(number);

    connect(thCaclHash,SIGNAL(changeValue(int,int)),this,SLOT(calcHashChangeValue(int,int)));
    connect(thCaclHash,SIGNAL(result(int,QString)),this,SLOT(calcHashResult(int,QString)));

    thCaclHash->start();
}

